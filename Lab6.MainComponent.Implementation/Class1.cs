﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.Display.Contract;

namespace Lab6.MainComponent.Implementation
{
    public class Class1 : Lab6.MainComponent.Contract.Interface1
    {
        private IDisplay d;
        public Class1(IDisplay d)
        {
            this.d = d;
        }
        public void M1()
        {
            Console.WriteLine("Metoda 1 z interface1");
        }

        public void M2()
        {
            Console.WriteLine("Metoda 2 z interface1");
        }
    }
}
