﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using PK.Container;

namespace ConsoleApplication1
{
    public class ContainerImplement : IContainer
    {
        private IDictionary<Type, Type> types = new Dictionary<Type, Type>();
        private IDictionary<Type, Object> rot = new Dictionary<Type, Object>();

        public void Register(Assembly assembly)
        {
            Type[] localTypes;
            localTypes = assembly.GetTypes();

            foreach (Type singleType in localTypes)
            {
                if (!singleType.IsNotPublic)
                {
                    this.Register(singleType);
                }
            }
        }

        public void Register(Type type)
        {
            Type[] localTypes = type.GetInterfaces();

            foreach (Type singleType in localTypes)
            {
                if (types.ContainsKey(singleType))
                {
                    types[singleType] = type;
                }

                else
                {
                    types.Add(singleType, type);
                }
            }
        }

        public void Register<T>(T impl) where T : class
        {
            var interfejsy = impl.GetType().GetInterfaces();

            for (int i = 0; i < interfejsy.Length; i++)
            {
                if (!rot.ContainsKey(interfejsy[i]))
                {
                    rot.Add(interfejsy[i], impl);
                }

                else
                {
                    rot[interfejsy[i]] = impl;
                }
            }
        }

        public void Register<T>(Func<T> provider) where T : class
        {
            Register(provider.Invoke() as T);
        }

        public T Resolve<T>() where T : class
        {
            return (T)Resolve(typeof(T));
        }

        public object Resolve(Type type)
        {
            if (rot.ContainsKey(type))
            {
                return rot[type];
            }

            else if (!types.ContainsKey(type))
            {
                return null;
            }

            else
            {
                ConstructorInfo[] konstruktory = types[type].GetConstructors();

                foreach (ConstructorInfo item in konstruktory)
                {
                    ParameterInfo[] info = item.GetParameters();

                    if (info.Length == 0)
                    {
                        return Activator.CreateInstance(types[type]);
                    }

                    List<object> lista = new List<object>(info.Length);

                    foreach (ParameterInfo pinfo in info)
                    {
                        var p = Resolve(pinfo.ParameterType);

                        if (p == null)
                        {
                            throw new UnresolvedDependenciesException();
                        }

                        lista.Add(p);
                    }

                    return item.Invoke(lista.ToArray());
                }
            }

            return null;
        }
    }
}
