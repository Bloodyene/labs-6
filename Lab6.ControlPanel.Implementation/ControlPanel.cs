﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab6.ControlPanel.Contract;
using Lab6.MainComponent.Contract;

namespace Lab6.ControlPanel.Implementation
{
    public  class ControlPanel : IControlPanel
    {
        private Panel panel = new Panel();

        private Interface1 i;
        public ControlPanel(Interface1 i)
        {
            this.i = i;
        }

        public System.Windows.Window Window
        {
            get { return panel; }
        }
    }
}
