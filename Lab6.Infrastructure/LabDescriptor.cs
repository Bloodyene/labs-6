﻿using Lab6.ControlPanel.Contract;
using PK.Container;
using System;
using System.Reflection;
using PK.Container;
using Lab6.ControlPanel.Implementation;
using Lab5.ContainerImpl;

namespace Lab6.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Func<IContainer> ContainerFactory = () => new ConsoleApplication1.ContainerImplement();

        public static Assembly ControlPanelSpec = Assembly.GetAssembly(typeof(IControlPanel));
        public static Assembly ControlPanelImpl = Assembly.GetAssembly(typeof(ControlPanel.Implementation.ControlPanel));

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(Lab6.MainComponent.Contract.Interface1));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(Lab6.MainComponent.Implementation.Class1));

        #endregion
    }
}
